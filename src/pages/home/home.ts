import { Component } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';
import { AuthPage } from './../auth/auth';
import { UserService } from './../../services/user.service';
import { ArticleListConfig } from './../../models/article-list-config.model';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  public listType:'all'|'feed';
  public isAuthenticated:boolean;
  listConfig = new ArticleListConfig();
  constructor(
    public navCtrl: NavController,
    private modalCtrl:ModalController,
    private userService:UserService
  ) {
  }
  ionViewDidLoad(){
    this.userService.isAuthenticated.subscribe(authenticated => {
      console.log(authenticated);
      this.isAuthenticated = authenticated;
      if(authenticated){
        this.listType = 'feed';
      }else{
        this.listType = 'all';
      }
      this.setListType();
    });
  }

  setListType(){
    this.setListTo(this.listType);
  }
  setListTo(type: string = '', filters: Object = {}) {
    // If feed is requested but user is not authenticated, redirect to login
    if (type === 'feed' && !this.isAuthenticated) {
      this.login();
      this.listType = 'all';
      return;
    }

    // Otherwise, set the list object
    this.listConfig = {type:type, filters:filters};
  }

  login(){
    let modal = this.modalCtrl.create(AuthPage, {
      isModal:true
    });
    modal.present();
  }
}
