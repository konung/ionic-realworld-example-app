import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';

import { ArticleListComponent } from './article-list/article-list.component';

@NgModule({
  imports:[
    CommonModule,
    IonicModule
  ],
  declarations:[
    ArticleListComponent
  ],
  exports:[
    ArticleListComponent
  ]
})
export class ComponentsModule{};
